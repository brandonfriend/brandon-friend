I taught myself to program at age 11 and fell in love with software development. I started taking computer science courses at Irvine Valley College at age 13 and never looked back.

Twenty years later, I've worked with top companies and spent years as a lead engineer at some of Orange County's hottest startups, including Acorns ($150M raised) and AutoGravity ($110M raised). My work in iOS has been featured by Apple in their commercials as well as in physical Apple stores across the country, and they have invited me to their campus to develop content that they later showcased at their developer conferences.

I've cofounded a number of my own startups and I regularly advise local startups on technology. My network includes top engineers, designers, and product managers in Orange County and Silicon Valley. I've absorbed cutting edge technology practices that I bring to every venture I join.

I love solving challenging problems with the best people. If you're doing something awesome, get in touch.

Brandon Friend has a Master's Degree in Mathematics from UC, Irvine.

Website: https://brandonfriend.com/
